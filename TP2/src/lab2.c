#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include "copy_mt.h"

/*rangeof(a,b,x), fonction qui renvoie un nombre réel, à x décimales, dans l'intervalle [a,b]
où a et b sont des nombres réels*/

double rangeof(double a, double b){
    return a + (b-a)*genrand_real1();
}

/*simul_avec_proba(n), programme qui renvoie une population de n individus regroupés en classes*/

void simul_with_fixed_proba(int n){

    int res[3] = {0,0,0}; //Nombres d'individus par classe
    double percentile[3]; //Pourcentage de chaque classe

    double nb_rand; //Stockage du nombre aléatoire

    for (int i=0 ; i<n ; i++){
        nb_rand = genrand_real1();
        if (nb_rand <= 0.35){
            res[0]++;
        }
        else{
            if (nb_rand <= 0.8){
            res[1]++;
            }
            else{
                res[2]++;
            }
        }
    }
    
    //Affichage du tableau (à mettre a part comme fonction réutilisable)

    for (int i=0 ; i<3 ; i++){
        printf("%d " , res[i]);
    }
    printf("\n");

    //Calcul et affichage du tableau pourcentage

    for (int i=0; i<3 ; i++){
        percentile[i] =  ( res[i] / (double) n ) * 100;
    }

    for (int i=0 ; i<3 ; i++){
        printf( "%f%% " , percentile[i] );
    }
    printf("\n");
}

/* simul_with_array(lg,array,n) : fonction qui prend un tableau d'entiers array de longueur x
et qui fait n simulations avec MT qui déinit les probabilités avec les observations*/

void simul_with_array(FILE *f,int lg, int * array, int n){

    double * proba = malloc(lg * sizeof(double));
    double * proba_cumul = malloc(lg * sizeof(double));

    double * simul_proba = malloc(lg * sizeof(double));
    int * simul = malloc(lg * sizeof(int)); //Résultat de la simulation
    

    int nb_tot = 0; // Nombre total d'observations
    int obs_cumul = 0; //Observations cumulés jusqu'à la classe i
    int j; //Compteur de la boucle while dans la partie Simulation

    double double_rand; // Stockage du nombre aléatoire


    //Initialisation de simul

    for (int i=0 ; i<lg ; i++){
        simul[i] = 0;
    }

    //Calcul du nombre total d'observations

    for (int i=0 ; i<lg ; i++){
        nb_tot += array[i];
    }

    //Calcul des probabilités

    for (int i=0 ; i<lg ; i++){
        proba[i] = array[i] / (double) nb_tot ;
    }

    //Calcul des probabilités cumulatives

    for (int i=0 ; i<lg ; i++){
        obs_cumul += array[i];
        proba_cumul[i] = obs_cumul / (double) nb_tot ; //Permet d'éviter les approx successives en réutilisant les probas
    }

    //Simulation

    for (int i=0 ; i<n ; i++){
        double_rand = genrand_real1();
        j=0;

        while (double_rand > proba_cumul[j]){
            j++;
        }

        simul[j] += 1;
    }

    for (int i=0 ; i<lg ; i++){

    }

    //Affichage du tableau

    //printf("Probabilités\n");
    for (int i=0 ; i<lg ; i++){
        fprintf(f,"%f " , proba[i]);
    }
    fprintf(f,"\n");

    //Cas où on print sur la console
    /*
    printf("Probabilités cumulatives\n");
    for (int i=0 ; i<lg ; i++){
        printf("%f " , proba_cumul[i]);
    }
    printf("\n");
    

    printf("Résultats de la simulation\n");
    */ 

    for (int i=0 ; i<lg ; i++){
        simul_proba[i] = (simul[i] / (double) n);
    }

    for (int i=0 ; i< lg ; i++){
        fprintf(f,"%f ", simul_proba[i]);
    }
    
}

//Question 4

double negExp(double mean){
    double rand_nb = genrand_real1(); 
    return (-1)*mean*log(1-rand_nb);
}

//Question 5_1

int simul_40_dice(){
    int sum = 0; //Oublié d'initialiser oupsi
    for (int i = 0 ; i < 40 ; i++){
        int int_gen = genrand_int31() % 6 + 1;
        sum += int_gen;
    }
    return sum;
}

//Question 5_2

void box_and_muller(FILE *f, int n){
    double rand1, rand2, x1, x2;
    int test20bins[20] = {0};
    double avg = 0, std_dev = 0;
    for (int i = 0 ; i < n ; i++){
        rand1 = genrand_real1();
        rand2 = genrand_real1();
        x1 = cos(2*M_PI*rand2)*pow((-2*log(rand1)),0.5);
        x2 = sin(2*M_PI*rand2)*pow((-2*log(rand1)),0.5);
        x1 = (x1 + 5) * 2; //Ramène un nombre entre -5 et 5 entre 0 et 20
        if (x1 < 20 && x1 > 0){
            test20bins[(int) x1]+=1;
        }
        x2 = (x2 + 5) * 2;
        if (x2 < 20 && x2 > 0){
            test20bins[(int) x2]+=1;
        }
    }
    for (int i = 0 ; i < 20 ; i++){
        fprintf(f,"%d ",test20bins[i]);
        //La moyenne et l'écart sont exprimées avec les bins
        avg += (((i/(double) 2) - 5) + 0.25)* test20bins[i];
    }
    avg = avg/n;
    for (int j = 0 ; j < 20 ; j++){
        std_dev += test20bins[j] * pow(((j/ (double) 2) - 5) - avg + 0.25,2);
    }
    std_dev = sqrt(std_dev/(double) n);
    fprintf(f,"Average : %f \n", avg);
    fprintf(f,"Deviation : %f \n", std_dev);

    
    
}

int main(void)
{

    //Partie fournie par Matsumoto
    FILE *f;
    f = fopen("out/test_MT.txt","w");
    int i;
    unsigned long init[4]={0x123, 0x234, 0x345, 0x456}, length=4;
    init_by_array(init, length);

    if (f!=NULL){
        fprintf(f,"1000 outputs of genrand_int32()\n");
        for (i=0; i<1000; i++) {
        fprintf(f,"%10lu ", genrand_int32());
        if (i%5==4) fprintf(f,"\n");
        }
        fprintf(f,"\n1000 outputs of genrand_real2()\n");
        for (i=0; i<1000; i++) {
        fprintf(f,"%10.8f ", genrand_real2());
        if (i%5==4) fprintf(f,"\n");
        }
    }
    fclose(f);
    //Test lab function 2

    printf("1000 températures entre -98 et 57.7 degrés Celsius");
    for (i=0;i<1000;i++){
        printf("%10.1f", rangeof(-98,57.7));
        if (i%6==5) printf("\n"); //Pris du code de Makoto, très utile à mettre
    }
    printf("\n");

    //Test lab function 3a

    int res[4] = {1000, 10000, 100000, 1000000};

    for (int j=0 ; j<4 ; j++){
        printf("Simulation avec %d individus \n", res[j]); //Compilé avec i, compilateur ne trouve pas d'erreur lol
        simul_with_fixed_proba(res[j]);
        printf("\n");
    }

    //Test lab function 3b
    f = fopen("out/question3_out.txt","w");
    int obs[6] = {100,400,600,400,100,200};
    if (f != NULL){
        for (int j=0 ; j<4 ; j++){
            fprintf(f,"Simulation avec %d individus \n", res[j]);
            simul_with_array(f,6,obs,res[j]);
            fprintf(f,"\n");
        }
    }
    fclose(f);

    //Test lab function 4a

    int test21bins[21] = {0}; //Tableau 4c
    double somme = 0;
    for (int j=0 ; j < 100000 ; j++){
        double nv_gen = negExp(10);
        if (nv_gen < 20){
            test21bins[(int) nv_gen]++;
        }
        else{
            test21bins[20]++;
        }
        somme += nv_gen;
    }
    for (int j=0 ; j<=20 ; j++){
        printf("%d ",test21bins[j]);
    }
    printf("\n");
    printf("Mean : %f\n", somme/100000);

    //Test lab function 5_1

    f = fopen("out/question_5_1.out","w");

    if (f != NULL){
        int test241bins[241] = {0};
        int somme_40 = 0; 
        double std_dev = 0;
        double avg;

        for (int j = 0 ; j<10000000 ; j++){ // 100000 is 'many' ? No.
            int f_dice = simul_40_dice();
            test241bins[f_dice]++;
            somme_40 += f_dice;
        }

        avg = somme_40/(double) 10000000;

        for (int j = 40 ; j<=240 ; j++){
            fprintf(f,"%d ",test241bins[j]);
            std_dev += test241bins[j] * pow((j - avg),2);
        }

        std_dev = sqrt(std_dev/(double) 10000000);

        fprintf(f,"\n");
        fprintf(f,"Somme : %d\n",somme_40);
        fprintf(f,"Average : %f\n", avg);
        fprintf(f,"Standard deviation : %f\n",std_dev );
    }
    fclose(f);

    //Test lab question 5_2

    f = fopen("out/question_5_2.out","w");
    
    if (f != NULL){
        box_and_muller(f,1000);
        fprintf(f,"\n");
        box_and_muller(f,1000000);
    }
    return 0;
}
