\babel@toc {nil}{}\relax 
\contentsline {section}{\numberline {1}Introduction}{3}{section.1}%
\contentsline {subsection}{\numberline {1.1}Objectif du TP }{3}{subsection.1.1}%
\contentsline {subsection}{\numberline {1.2}Mersenne-Twister }{3}{subsection.1.2}%
\contentsline {paragraph}{Note :}{3}{section*.2}%
\contentsline {subsection}{\numberline {1.3}Vérification de la portabilité de Mersenne-Twister}{3}{subsection.1.3}%
\contentsline {section}{\numberline {2}Génération de nombres uniformes entre a et b}{4}{section.2}%
\contentsline {paragraph}{Note :}{4}{section*.3}%
\contentsline {paragraph}{Note 2 :}{4}{section*.4}%
\contentsline {section}{\numberline {3}Reproduction d'une distribution aléatoire à partir d'une population}{4}{section.3}%
\contentsline {paragraph}{Note :}{5}{section*.5}%
\contentsline {section}{\numberline {4}Distributions aléatoires continues}{5}{section.4}%
\contentsline {subsection}{\numberline {4.1}Avec une fonction réversible}{6}{subsection.4.1}%
\contentsline {paragraph}{Note :}{6}{section*.6}%
\contentsline {paragraph}{Note :}{7}{section*.7}%
\contentsline {subsection}{\numberline {4.2}Avec une fonction non réversible}{7}{subsection.4.2}%
\contentsline {subsubsection}{\numberline {4.2.1}Avec des dés}{7}{subsubsection.4.2.1}%
\contentsline {subsubsection}{\numberline {4.2.2}Avec Box et Muller}{8}{subsubsection.4.2.2}%
\contentsline {section}{\numberline {5}Conclusion}{8}{section.5}%
