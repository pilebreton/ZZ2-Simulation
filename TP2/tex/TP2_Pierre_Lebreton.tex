\documentclass[french]{article}
\usepackage{babel}
\usepackage{hyperref}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{listings}
\usepackage{caption} 

\newcommand{\n}{\newline}

\title{TP2-Génération de nombres aléatoires}
\author{Pierre Lebreton, Encadrant : David Hill}
\date{8 octobre 2024}

\begin{document}

\maketitle

\pagebreak

\tableofcontents

\pagebreak

\section{Introduction}

\subsection{Objectif du TP }
Le but de ce TP est de manipuler des générateurs aléatoires corrects pour la science. Dans ce TP, nous utiliserons Mersenne-Twister pour implémenter certaines distributions aléatoires.
\n
Dans ce TP, on va essayer d'implémenter :
\begin{itemize}
	\item Une génération de nombres aléatoires entre a et b
	\item Une génération de nombres aléatoires suivant une distribution suivant une population
	\item Une génération de nombres aléatoires à partir d'une fonction continue (réversible ou non)
\end{itemize}

\subsection{Mersenne-Twister }

Mersenne-Twister est un générateur aléatoire développé par Makoto Matsumoto et Takuji Nishimura en 1997. Il passe la plupart des tests statistiques pour les générateurs aléatoires, ce qui en fait un bon générateur aléatoire. Pour l'initialiser, on utilise un vecteur maximal de 19937 bits. Pour ce TP, on utilisera le vecteur fourni par Matsumoto dans la version avec initialisation de 2002, utilisant le vecteur à 4 entiers suivant :
\n
$ v = \lbrace 0$x$123 , 0$x$234, 0$x$345 , 0$x$456 \rbrace $
\n
Cette initialisation permet la reproductibilité des expériences.

\paragraph{Note :}
Partager la même initialisation est très pratique pour vérifier les résultats avec les copains d'à côté.

\subsection{Vérification de la portabilité de Mersenne-Twister}

Matsumoto fournit avec son code C de Mersenne-Twister avec des tests et des résultats attendus, ce qui est très utile, il suffit d'exécuter le code, de mettre le résultat dans un fichier .txt, et utiliser la commande bash \textit{diff} entre les deux fichiers.
\n
La commande retournant les différences des deux fichiers ligne à ligne, le résultat attendu dans la console est :

\lstset{language=bash}

\begin{lstlisting}
$ diff Code_MT/mt19937ar.out experimental_out.txt
>
\end{lstlisting}

Oui, rien du tout, car il n'y a aucune différence entre les fichiers. On peut donc sereinement commencer le reste du TP, en sachant que notre générateur de nombre entiers et réels est fiable.

\section{Génération de nombres uniformes entre a et b}

Premier exercice plutôt facile, tirer un nombre aléatoire uniforme entre a et b à partir d'un générateur aléatoire entre 0 et 1. On utilise la formule pour calculer tous les points d'un segment entre a et b, comme dans un espace vectoriel.
\n
$$\forall x \in \left[ 0,1 \right] , z \in \left[ AB \right]~si~z = A + (B-A) \times x$$
\n
\n
Le x est substitué par notre générateur aléatoire entre 0 et 1, ce qui donne un nombre uniforme entre a et b.

\paragraph{Note :}
Du coup, la précision du générateur aléatoire est divisée par $(b-a)$ dû à la multiplication, notre ordinateur ne possédant pas une précision infinie.

\paragraph{Note 2 :}
Attention à utiliser des double aussi, c'est un moyen simple d'améliorer la précision.

\section{Reproduction d'une distribution aléatoire à partir d'une population}

Cette partie s'intéresse au problème suivant : on possède une population répartie en classes. On cherche à en extraire un programme donnant une classe aléatoire parmi celles de la population selon les probabilités données par la population. Par exemple, si une classe A représente $35 \% $ de la population, notre programme renverra A avec $35 \% $ de chance.
\n
\n
En vérité, notre programme prend en entrée un \textit{array}, liste de la population à simuler, et un entier \textit{n} un nombre d'individus à simuler. Notre programme affichera un "tableau" avec le nombre d'individus dans chaque classe sur la console. Il est aussi possible de mettre ces informations dans un fichier, pour l'utiliser pour faire un graphique sous Excel ou LibreOffice Calc.
\n
\n
Pour faire ce programme, on a 3 étapes :
\n
\begin{enumerate}
	\item On calcule le nombre total d'observations avec lequel on travaille
	\item On fait un tableau des probabilités cumulées de chaque classe.
	\item On créé une boucle while qui tire la classe aléatoirement à l'aide des probabilités cumulées.
\end{enumerate}

\begin{center}
	\includegraphics[scale=2.5]{../img/graphe3.png}
	\captionof{figure}{Graphique question 3}
	\label{fig1}
\end{center}

\begin{itemize}
	\item Colonne bleue : Probabilités attendues
	\item Colonne orange : $n = 1000$
	\item Colonne jaune : $n = 10000$
	\item Colonne verte : $n = 100000$
	\item Colonne bordeaux : $n = 1000000$
\end{itemize}

On voit que pour $n = 1000$, on a des différences entre les probabilités de la population d'entrée et celle simulée visibles. Pour $n = 1000000$, on ne remarque aucune différence sur le graphique.

\paragraph{Note :} 
LibreOffice a comme convention utiliser les nombres flottants avec une virgule, le C par un point, la conversion se fait à l'aide d'un \textit{CRTL + F} dans LibreOffice.

\section{Distributions aléatoires continues}

Dans cette section, nous étudierons la génération de nombres aléatoires à l'aide d'une distribution continue. On possède une fonction $f$, et on cherche un nombre aléatoire $z$ tel que :
\n
$$P(z \le T) = \int_{0}^{z} f(x) dx$$

Ou même, plus simplement :
\n
$$P(z \le T) = f(z)$$

\subsection{Avec une fonction réversible}

Pour effectuer cela, on peut voir que $P(z \le T)$ est un nombre entre 0 et 1, donc tirable par Mersenne-Twister, si $f$ est intégrable sur $\left[ 0, z \right]$ et son intégrale réversible, on peut, en notant $F$ la primitive de $f$ :
\n
$$z = F^{-1}(RandNumber)$$
\n
\n
Pour illustrer cela, il est demandé de coder ce prinipe pour l'exponentielle négative, avec une moyenne de $10$. Pour être plus général, on le fera pour n'importe quelle moyenne $M$.
\n
\n
On représentera le nombre entre $0$ et $1$ par $r$.
\n
\n
$r = 1 - e^{- \frac{1}{M} x}$
\n
$\Longrightarrow 1 - r = e^{- \frac{1}{M} x}$
\n
$\Longrightarrow ln(1 - r) = - \frac{1}{M} x$
\n
$\Longrightarrow x = -M \times ln(1 - r)$

\paragraph{Note :}
Merci la diapositive 52 du C3
\n
Pour vérifier notre distribution, on créé un tableau destiné à collecter les nombres aléatoires $x$ générés, et les comparer avec la distribution théorique, d'où la Figure 2.
\begin{center}
	\includegraphics[scale=2.2]{../img/graphe4.png}
	\captionof{figure}{Graphique question 4}
	\label{fig2}
\end{center}

On vérifie également que la moyenne expérimentale est très proche de la moyenne théorique \textit{(cf. code source)}

\paragraph{Note :}
Ce fichier LibreOffice a été rempli à la main, mais on peut comme le premier, le remplir à l'aide d'un fichier .txt, sortie de notre programme, puis utiliser le format de collage spécial de LibreOffice.

\subsection{Avec une fonction non réversible}

Dans cette section, on cherche à générer des nombres aléatoires à l'aide de fonctions en cloches, donc non réversibles.

\subsubsection{Avec des dés}

La première étape est d'effectuer cela à l'aide de lancer de dés. Lors du lancer de plusieurs dés, si le nombre de dés tend vers $+ \infty$, alors la distribution converge vers une courbe en cloche. L'essai sur le TP est demandé avec 40 dés. Voici le résultat sous forme de graphique.

\begin{center}
	\includegraphics[scale=2.5]{../img/graphe5.png}
	\captionof{figure}{Graphique question 5.1}
	\label{fig3}
\end{center}

On obtient également les paramètres de cette courbe en cloche, une moyenne $ M = 140$, et un écart-type $\sigma = 10.8$

\pagebreak

\subsubsection{Avec Box et Muller}

Dans cette deuxième méthode, on utilise une méthode qui consiste à générer deux nombres pseudo-aléatoires pour tirer deux nombres, un de part et d'autre de la courbe, $x_1$ et $x_2$.
\n
$$x_1 = cos(2 \pi r_2)(- 2 ln (r_1))^{\frac{1}{2}}$$
\n
$$x_2 = sin(2 \pi r_2)(- 2 ln (r_1))^{\frac{1}{2}}$$


\begin{center}
	\includegraphics[scale=2.5]{../img/graphe5_2.png}
	\captionof{figure}{Graphique question 5.2}
	\label{fig4}
\end{center}

On obtient en moyenne approximative $ M = 0$, et $\sigma = 1.4$. La moyenne $M$ est normale mais l'écart-type est anormal s'il s'agit de la loi centrée réduite.

\section{Conclusion}

Dans ce TP, nous avons manipulé des générateurs de nombres pseudo-aléatoires fiables dans un contexte scientifique, afin d'en créer de nouveaux plutôt fiables également. Nous avons appris à concevoir des générateurs aléatoires de nombres entiers à l'aide d'une population, et des nombres réels à partir d'une fonction de distribution. 
\n
En plus, nous avons fait tout cela en conservant la reproducibilité de l'expérience dû au fait que nous avons gardé le même état d'initialisation.
\n
Cela m'a permis personnellement de commencer à adopter une meilleure hygiène du code, même si largement perfectible,car il manque des cartouches de fonction clairs, pas faits par manque de temps,et la séparation des fonctions et du main. \textit{(cf. code source)}

\end{document}