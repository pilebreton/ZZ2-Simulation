#include <stdio.h>
#include <string.h>

void rabbit_evolution(int nbMonths){
    int adult_rabbits = 1;
    int young_rabbits = 0;
    for (int i =  1 ; i <= nbMonths ; i++){
        int growing_rabbits = young_rabbits; //Nb of rabbits becoming adults this month
        young_rabbits = adult_rabbits; //New baby rabbits
        adult_rabbits = adult_rabbits + growing_rabbits;
        fprintf(stdout,"Number of rabbits, month n°%d : %d\n", i, adult_rabbits + young_rabbits);
    }
}

int main(){
    rabbit_evolution(18);
}