//import org.apache.commons.math3.random.BitsStreamGenerator;
//import org.apache.commons.math3.random.MersenneTwister;

import java.lang.Math;

/*
 * class OldRabbit
 * 
 * Etat d'un vieux Lapin, qu'il garde de ses 5 ans à sa mort.
 * Plus de reproduction possible, taux de mortalité augmenté chaque mois
 * Pour arriver à un taux de survie de 60% - 10% pour chaque année dans l'état
 * Vieux
 */
public class OldRabbit extends State{

    /*
     * Méthode nextMonth()
     * 
     * Teste si le Lapin reste en vie, si oui augmente son âge, sinon
     * le met dans deadRabbitsThisMonth en attendant sa suppression
     */
    public void nextMonth(Rabbit r, Population pop){
        double survival_rate = Math.pow(0.6 - ( Math.floor(r.age/12.0 - 5.0) )* 0.1 , 1/12.0);
        if (Math.random() >= survival_rate){
            //System.out.println("Lapin rajouté au cimetière\n"); //Bavard
            pop.deadRabbitsThisMonth.add(r);
        }
        else{
            r.age++;
        }
    }

    @Override
    public void affichage(){
        System.out.println("Vieux lapin");
    }
}