/*
 * class YoungRabbit
 * 
 * Etat d'un Lapin entre sa naissance et ses 8 mois
 * Peut juste grandir et pas se reproduire encore
 * Possède un taux de mortalité de 65% lors de sa première année
 * 
 */


public class YoungRabbit extends State{
    @Override
    /*
     * Méthode nextMonth()
     * 
     * Vérifie si ce Lapin meurt ce mois-ci, dans ce cas, le met dans
     * deadRabbitsThisMonth. Si l'âge passe au-dessus de 8 mois, change
     * l'état en Lapin Adulte.
     * 
     */
    public void nextMonth(Rabbit r, Population pop){
        double survival_rate = Math.pow(0.35 , 1 / 12.0);
        if (Math.random() >= survival_rate){
            //System.out.println("Lapin rajouté au cimetière\n"); //Bavard
            pop.deadRabbitsThisMonth.add(r);
        }
        else{
            r.age++;
            if (r.age >= 8){
                r.state = new AdultRabbit();
            }
        }
    }

    @Override
    public void affichage(){
        System.out.println("P'tit lapin");
    }
}