// "Il n'y a rien de plus dangereux dans cette forêt que le lapin adulte. Je le jure, Bohort, je le jure" - Arthur Pendragon

/*
 * class AdultRabbit
 * 
 * Etat d'un lapin qu'il conserve de ses 8 mois à ses 5 ans.
 * Période pendant laquelle les femelles peuvent se reproduire.
 * Possède un taux de mortalité de 40% chaque année
 * Une lapine peut enfanter seule, pas de gestion de 'couple de lapins'
 * Nombre de portées par année : de 3 à 9, moyenne de 6
 * Nombre de lapins par portée : de 3 à 6, équiprobable
 * 
 */
public class AdultRabbit extends State{

    /*
     * Méthode nextMonth()
     * 
     * Vérifie si le Lapin meurt ce mois-ci, auquel cas le met
     * dans deadRabbitsThisMonth. Si l'âge passe au-dessus de 5 ans,
     * le met dans l'état OldRabbit
     * 
     */
    public void nextMonth(Rabbit r, Population pop){
        double survival_rate = Math.pow(0.6 , 1/12.0); //Gnnnnnnnn, quelque chose puissance 1/12 entier ça fait 0
        if (Math.random() >= survival_rate){
            //System.out.println("Lapin rajouté au cimetière\n"); //Bavard
            pop.deadRabbitsThisMonth.add(r);
        }
        else{
            r.age++;
            if (r.age >= 5*12){
                r.state = new OldRabbit();
            }
            else{
                if (r.gender == 1){
                    start_year(r);
                    double nb_nv_lapins = give_birth(r);
                    for (int i = 0 ; i < nb_nv_lapins ; i++){
                        Rabbit rptit = new Rabbit();
                        pop.newRabbitsThisMonth.add(rptit);
                    }
                }
                else{
                    //Do nothing
                }
            }
        }
    }

    /*
     * Méthode start_year()
     * 
     * Procédure qui permet de réinitialiser les portées annuelles des
     * lapines adultes, appelées tous les mois, mais modifiant la variable
     * que tous les 12 mois où lors du passage à l'âge adulte.
     *  Modifie l'attribut du lapin beginning_month
     * 
     */
    public void start_year(Rabbit r){ //Devrait être appellée que pour des femelles
        if (r.beginning_month == 0 || (r.age - 12 == r.beginning_month)){ //Si le lapin vient de passer à l'âge adulte, ou fin de l'année en cours
            r.beginning_month = r.age;
            r.litters = Math.floor(Math.random()*4 + 1) + Math.floor(Math.random()*4 + 1) + 1; //2d4 + 1 for RPG nerds
        }
    }

    /*
     * Méthode give_birth()
     * 
     * Fonction qui renvoie le nombre de lapins mis bas, entre 3 et 6
     * qui indique si la lapine donne naissance ce mois-là ou pas.
     * Si c'est le cas, diminue le nombre de portées restantes.
     * Fait en sorte que la lapine donne toujours le nombre exact de
     * portées prévues pour l'année grâce à la formule de proba suivante : 
     * nombre de portées restantes sur le nombre de mois de l'année restante
     */
    public double give_birth(Rabbit r){ //Appelée que sur des lapines
        boolean is_giving_birth = (Math.random() <= ((float) r.litters / (12 + r.beginning_month - r.age))); //Formule décrite
        double nb_lapins;
        if (is_giving_birth){
            r.litters--;
            nb_lapins = Math.floor(Math.random()*4 + 1) + 3 ; // 1d4 + 3 for RPG nerds 
        }
        else{
            nb_lapins = 0;
        }
        return nb_lapins;
    }

    @Override
    public void affichage(){
        System.out.println("Lapin adulte");
    }
}