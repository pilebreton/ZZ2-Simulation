/*
 * class Rabbit
 * 
 * Cette classe créé les objets principaux qu'on manipule lors de cette évolution
 * On stocke l'âge de la bête, son genre, le nombre de portées restantes ce mois-ci
 * et l'état (Jeune, Adulte ou Vieux) du lapin concerné
 */

public class Rabbit{
    int age; //Age en mois
    double gender; //1 pour femelle, 2 pour mâle
    double litters; //0 pour les mâles, nombre de portées restantes cette année pour les femelles
    int beginning_month; // Mois où l'année de portée a commencé pour ce lapin
    State state; //Etat du Lapin
    static int compteur;
    int id; // Id du Lapin

    /*
     * Constructeur Rabbit()
     * 
     * Créé un Lapin de genre aléatoire 50/50 (si compté que Math.random() marche)
     * jeune âgé de 0 mois
     */
    public Rabbit(){

        age = 0;
        gender = Math.floor(Math.random()* 2 + 1);
        litters = 0;
        beginning_month = 0;
        state = new YoungRabbit(); // Etat Jeune Lapin
        id = compteur;
        compteur++;

        //System.out.println("Lapin créé normalement \n"); //Bavard
    }
    
    /*
     * Constructeur Rabbit()
     * 
     * Créé un lapin comme le premier constructeur mais avec un genre défini
     * Utilisé uniquement pour la mise en place des couples de départ
     * @param gender
     * 
     */
    public Rabbit(double gender){
        age = 0;
        this.gender = gender;
        litters = 0;
        beginning_month = 0;
        state = new YoungRabbit(); //Etat Jeune Lapin
        id = compteur;
        compteur++;
        //System.out.println("Lapin de début \n"); //Bavard
    }

    /*
     * Méthode nextMonth()
     * 
     * Fait évoluer l'état du Lapin pour ce mois, augmente
     * son âge, et sa reproduction ou mort selon son état
     * 
     * 
     */
    public void nextMonth(Population pop){
        state.nextMonth(this, pop); //
    };

    /*
     * Méthode affichage()
     * 
     * Méthode utilisée et codée pour aide au déboggage,
     * elle a été laissée au cas-où elle pourrait être utile 
     * 
     */
    public void affichage(){
        System.out.println(age + " " + gender + " " + beginning_month + " " + litters + " ");
        state.affichage(); //Affiche l'état du Lapin
    }
}