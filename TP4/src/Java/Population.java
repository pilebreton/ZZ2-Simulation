import java.util.ArrayList;
import java.util.List;

/*
 * class Population
 * 
 * Cette classe permet l'évolution d'une colonie de lapins
 * Ses attributs sont des listes de lapins, qui permet de les stocker et de les déplacer
 * On peut les voir comme des clapiers quasi infini !!!
 * 
 * Les listes newRabbitsThisMonth et deadRabbitsThisMonth permettent de stocker les lapins à déplacer ce mois-ci
 * (à savoir naissance et mort)
 * Cela permet de ne pas modifier la liste pop, celle utilisée pour l'évolution, pendant son parcours par l'itérateur, voir
 * méthode nextMonth()
 */
public class Population{
    List<Rabbit> pop;
    List<Rabbit> newRabbitsThisMonth;
    List<Rabbit> deadRabbitsThisMonth;

    //Getter

    /*
     * Méthode getNewRabbits()
     * Simple getter des nouveaux-nés de ce mois
     * @return newRabbitsThisMonth
     * 
     */
    List<Rabbit> getNewRabbits(){
        return newRabbitsThisMonth;
    }
    
    /*
     * Méthode getDeadRabbits()
     * Simple getter des morts de ce mois
     * @return ndeadRabbitsThisMonth
     * 
     */
    List<Rabbit> getDeadRabbits(){
        return deadRabbitsThisMonth;
    }

    /*
     * Constructeur Population()
     * Initialise les trois attributs par une liste vide
     * L'instance de Population est prête pour une simulation
     */
    public Population(){
        pop = new ArrayList<>();
        newRabbitsThisMonth = new ArrayList<>();
        deadRabbitsThisMonth = new ArrayList<>();
    }

    /*
     * Méthode resetPopulation()
     * 
     * Permet de vider les trois listes de l'instance de Population
     * 
     */
    public void resetPopulation(){
        
        pop.clear();
        newRabbitsThisMonth.clear();
        deadRabbitsThisMonth.clear();
    }

    /*
     * Méthode nextMonth()
     * 
     * Méthode principale de cette classe, permet d'itérer la liste de lapins pop,
     * et de les faire évoluer un par un
     */
    public void nextMonth(){

        for (Rabbit rab : pop){
            rab.nextMonth(this);
        }

        /* Gestion des naissances */
        pop.addAll(newRabbitsThisMonth);
        newRabbitsThisMonth.clear();

        /* Gestion des morts */
        for (Rabbit rab : deadRabbitsThisMonth){
            pop.remove(rab);
            //System.out.println("Lapin mort\n"); //Bavard
        }
        deadRabbitsThisMonth.clear();
    }

}