/* Simulation
 * 
 * Classe abstraite. Contient la méthode main du programme de simulation d'une colonie de lapin.
 * 
 * 
 */



public abstract class Simulation {

    /*
     * Méthode simuler
     * 
     * Cette méthode simule nb_simu évolutions de populations de lapins, de nb_months chacun
     * avec starting_couples couples de départ, elle affiche le nombre de lapins de chaque
     * population à la fin de la simulation sur la console.
     * 
     * @param starting_couples Entier, nombre de couples de lapins, nouveaux-nés, au début de chaque simulation
     * @param nb_months Entier, nombre de mois d'évolution de chaque population
     * @param nb_simu Nombre de populations à simuler
     * 
     */
    public static void simuler(int starting_couples, int nb_months, int nb_simu){
        Population p = new Population();
        for (int k = 0 ; k < nb_simu ; k++){
            for (int i = 0 ; i < starting_couples ; i++){
                Rabbit fr = new Rabbit(1);
                Rabbit mr = new Rabbit(2);
                p.pop.add(fr);
                p.pop.add(mr);
            }
            for (int i = 0 ; i < nb_months ; i++){
                p.nextMonth();
            }
            System.out.println(p.pop.size());
            p.resetPopulation();
        }
    }

    public static void main(String[] args){
        simuler(5,65,100);
    }
}
