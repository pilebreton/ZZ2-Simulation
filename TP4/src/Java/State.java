/*
 * Interface State
 * 
 * Mise en place du State pattern pour le Lapin
 * Pas prévu pour être instanciée, a trois classes
 * implémentant cette interface, OldRabbit, AdultRabbit,
 * et YoungRabbit
 * 
 * 
 */


public abstract class State{

    /*
     * Méthode nextMonth()
     * 
     * Overridée par les classes l'implémentant
     * Permet de faire évoluer le lapin r, et de changer son état
     * 
     */
    public void nextMonth(Rabbit r, Population pop){};

    /*
     * Méthode affichage()
     * 
     * Affiche l'état sous forme Texte
     * Overridée par les classes l'implémentant
     * 
     */
    public void affichage(){};
}