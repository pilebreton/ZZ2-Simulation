#include "Code_MT/mt19937ar.h"
#include <stdio.h>
#include <stdlib.h>
#include "math.h"

//SimulPi est une fonction qui prend en entrée un entier n, nombre de points à tirer
double simulPi(int n){
    double rand1,rand2;
    int compt_int = 0;
    for (int i = 0 ; i < n ; i++){
        rand1 = genrand_real1();
        rand2 = genrand_real1();
        if (rand2 < sqrt(1 - pow(rand1,2))){
            compt_int++;
        }
    }
    return (compt_int/(double) n)*4;
}

//ind_exp est une fonction qui prend en entrée nbExp, un nombre d'expériences, et NbPoints, nombre de points par expérience
double ind_exp(int nbExp, int nbPoints){
  double * tab = malloc(nbExp*sizeof(double));
  double avg = 0;
  for (int i = 0 ; i < nbExp ; i++){
    tab[i] = simulPi(nbPoints);
  }
  for (int i = 0 ; i < nbExp ; i++){
    avg += tab[i];
  }
  avg = avg/(double) nbExp;
  return avg - M_PI;
}

//Confiance_intervals
void conf_intervals(int nbPoints){
  double tab[40] = {0};
  int nbExp = 40;
  double avg = 0;
  double std_dev = 0;
  for (int i = 0 ; i < nbExp ; i++){ //Simulation et estimation de PI
    tab[i] = simulPi(nbPoints);
  }
  for (int i = 0 ; i < nbExp ; i++){ //Calcul de la moyenne
    avg += tab[i];
  }
  avg = avg/(double) nbExp;

  for (int i = 0 ; i < nbExp ; i++){ //Calcul de la déviation
    std_dev += pow(tab[i] - avg, 2);
  }
  std_dev = std_dev / (nbExp - 1);
  double radius = 2.021*sqrt(std_dev/nbExp);

  printf("Intervalle : [%f,%f]\n", avg - radius, avg + radius);
 
}

int main(void)
{ 
    //Initialisation de Mersenne-Twister
    int i;
    unsigned long init[4]={0x123, 0x234, 0x345, 0x456}, length=4;
    init_by_array(init, length);

    //Test lab 3 question 1

    int obs[3] = {1000,100000,1000000};
    for (i = 0 ; i < 3 ; i++){
      double pi = simulPi(obs[i]);
      printf("%f\n",pi);
    }

    //Test lab3 question 2

    for (i = 0 ; i < 3 ; i++){
      printf("Average pour %d itérations avec %d expériences : %f\n", obs[i], 40, ind_exp(40, obs[i]));
    }

    //Test lab3 question 3
    for (i = 0 ; i < 3 ; i++){
      conf_intervals(obs[i]);
    }

    return 0;
}